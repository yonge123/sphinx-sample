======================
Help
======================


|
|
|


What is Sphinx ?
-----------------------

Sphinx is a tool that makes it easy to create intelligent and beautiful documentation, 
written by Georg Brandl and licensed under the BSD license.
It was originally created for the new Python documentation, 
and it has excellent facilities for the documentation of Python projects, 
but C/C++ is already supported as well, and it is planned to add special support for other languages as well.

Sphinx 는 지능적이고 아름다운 문서를 쉽게 만들 수있는 도구입니다

Sphinx 는 reStructuredText 문서를 테마에 맞게 HTML 로 만들어주는 도구입니다.

Sphinx 는 Python 코드 내에 들어간 docstring을 자동으로 문서화 해주는 도구입니다.


Reference

- `<http://www.sphinx-doc.org/en/master/>`_


|
|
|


Why use Sphinx ?
-----------------------

1. rst 문선 만 작성하면 여러가지 포맷으로 컨퍼팅 할 수 있다는 점. (html, pdf, md, blogger, 등)

2. markdown 보다 확장된 기능들





Reference

- https://thomas-cokelaer.info/tutorials/sphinx/introduction.html

- http://openmetric.org/sphinx-notebook/ch-intro/index.html



|
|
|


How to use ?
--------------------

|

Install Sphinx
``````````````````

.. code-block:: python

    pip install sphinx


Getting Started
'''''''''''''''''''

- reference

    https://gisellezeno.com/tutorials/sphinx-for-python-documentation.html
    https://dandyrilla.github.io/2017-09-07/python-sphinx/
    https://tech.ssut.me/start-python-documentation-using-sphinx/
    http://www.hanul93.com/python-sphinx/


Once Sphinx is installed, you can proceed with setting up your first Sphinx project. 
To ease the process of getting started, Sphinx provides a tool, ``sphinx-quickstart``, 
which will generate a documentation source directory and populate it with some defaults. 
Weâ€™re going to use the sphinx-quickstart tool here, though itâ€™s use by no means necessary.


0. windows Cmd 창 열기
...........................

1. 우선 D 드라이브에 docs 폴더를 생성합니다.
...................................................

::

    d:
    mkdir docs
    cd docs

2. sphinx-quickstart 을 이용해 문서 생성에 필요한 파일들을 자동 생성합니다.
.....................................................................................

- 질문 과 답변으로 몇가지 설정을 하고 나면 make html 코멘드로 문서를 생성 할 수 있습니다.

::

    sphinx-quickstart
    
    You have two options for placing the build directory for Sphinx output.
    Either, you use a directory "_build" within the root path, or you separate
    "source" and "build" directories within the root path.
    > Separate source and build directories (y/n) [n]: y

    Inside the root directory, two more directories will be created; "_templates"
    for custom HTML templates and "_static" for custom stylesheets and other static
    files. You can enter another prefix (such as ".") to replace the underscore.
    > Name prefix for templates and static dir [_]:

    The project name will occur in several places in the built documentation.
    > Project name: ksg
    > Author name(s): kk
    > Project release []: 1.0

    If the documents are to be written in a language other than English,
    you can select a language here by its language code. Sphinx will then
    translate text that it generates into that language.

    For a list of supported codes, see
    http://sphinx-doc.org/config.html#confval-language.
    > Project language [en]:

    The file name suffix for source files. Commonly, this is either ".txt"
    or ".rst".  Only files with this suffix are considered documents.
    > Source file suffix [.rst]:

    One document is special in that it is considered the top node of the
    "contents tree", that is, it is the root of the hierarchical structure
    of the documents. Normally, this is "index", but if your "index"
    document is a custom template, you can also set this to another filename.
    > Name of your master document (without suffix) [index]:
    Indicate which of the following Sphinx extensions should be enabled:

    > autodoc: automatically insert docstrings from modules (y/n) [n]: y
    > doctest: automatically test code snippets in doctest blocks (y/n) [n]:
    > intersphinx: link between Sphinx documentation of different projects (y/n) [n]: y
    > todo: write "todo" entries that can be shown or hidden on build (y/n) [n]: y
    > coverage: checks for documentation coverage (y/n) [n]:
    > imgmath: include math, rendered as PNG or SVG images (y/n) [n]:
    > mathjax: include math, rendered in the browser by MathJax (y/n) [n]:
    > ifconfig: conditional inclusion of content based on config values (y/n) [n]:

    > viewcode: include links to the source code of documented Python objects (y/n) [n]: y

    > githubpages: create .nojekyll file to publish the document on GitHub pages (y/n) [n]:

    A Makefile and a Windows command file can be generated for you so that you
    only have to run e.g. `make html' instead of invoking sphinx-build
    directly.
    > Create Makefile? (y/n) [y]:
    > Create Windows command file? (y/n) [y]:


- Separate source and build directories (y/n) [n]: ``y``
- Project name: ``Sphinx-GUide``
- Author name(s): ``Toiion``
- Project release []: ``1.0``
- autodoc: automatically insert docstrings from modules (y/n) [n]: ``y``
- intersphinx: link between Sphinx documentation of different projects (y/n) [n]: ``y``
- todo: write "todo" entries that can be shown or hidden on build (y/n) [n]: ``y``
- viewcode: include links to the source code of documented Python objects (y/n) [n]: ``y``
- others options: ``Enter``


3. 아래와같이 파일들이 생성 된것을 확인 할 수 있습니다.
...................................................................

::

    D:/sphinxEx/docs|
                    |build
                    |source|_static
                    |source|-templates
                    |source|conf.py
                    |source|index.rst
                    |make.bat
                    |Makefile


4. D:/sphinxEx/docs/source/sample.rst  파일 을 만들고 아래 내용을 저장합니다.
................................................................................................................

::

	==============================
	Sample
	==============================

	Test
	----------------

	#. Item 1.

	#. Item 2.

	More text.

	#. Part A::

			command block

		OR::

			alternate command block

	#. Part B.

	#. Item 3. This item can have a long paragraph across multiple lines. One two
	three four five six seven eight nine ten eleven twelve thirteen fourteen
	fifteen sixteen seventeen eighteen nineteen twenty.

	- Option A::

			command block

	- Option B::

			alternate command block

5. D:/sphinxEx/my_package/my_module.py 파일 생성
.....................................................................................................

.. code-block:: python

    # -*- coding: utf-8 -*-
    """
        My Module
        ~~~~~~~~~
    """

    class Math(object):
        """두 개의 int 값을 입력받아 다양한 연산을 할 수 있도록 하는 클래스.
        
        :param int a: a 값
        :param int b: b 값
        """

        def __init__(self, a, b):
            self._a = a
            self._b = b

        def sum(self):
            """미리 입력받은 a와 b값을 더한 결과를 반환합니다.

            예제:
                다음과 같이 사용하세요:

                >>> Math(1, 2).add()
                3
            
            :returns int: a + b에 대한 결과
            """
            return self._a + self._b
        
        def subtract(self):
            """미리 입력받은 a와 b값을 뺀 결과를 반환합니다.

            예제:
                다음과 같이 사용하세요:

                >>> Math(2, 1).subtract()
                1

            :returns int: a - b에 대한 결과
            """
            return self._a - self._b


6. conf.py 파일 에서 아래 python code 추가 
...................................................

.. code-block:: python

    import os
    import sys
    sys.path.insert(0, os.path.abspath('../..'))


7. sphinx-apidoc -f -o source/ ../my_package/   실행 하여 py 파일에해당 되는  rst 파일 생성
......................................................................................................

::

    D:/sphinxEx/docs|
                    |source|modules.rst
                    |source|my_package.rst



8. D:/sphinxEx/docs/source/index.rst  파일 을 아래 와 같이 수정 합니다.
........................................................................................................................

::

    Welcome to test's documentation!
    ================================

    .. toctree::
        :maxdepth: 2

        sample

        modules


9. make html 으로  html  생성.
...................................

::

    make html


D:/sphinxEx/docs/build/html/index.html 를 싷행 하시면 아래와 같이 보입니다.


.. figure::  _sources/images/index_page_capture.jpg
    :align:   center

    index.html

|

Sample.html

.. figure::  _sources/images/sample_page_capture.jpg
    :align:   center

    sample.html



|
|
|

-------


Theme
``````````````

|

- references
    - https://sphinx-themes.org/
    - https://www.writethedocs.org/guide/tools/sphinx-themes/


|

-----------------

install theme
''''''''''''''''''

.. code-block:: python

    pip install sphinx_rtd_theme

|

-----------------

set theme 
'''''''''''''''

edit the  ``conf.py`` file to change the theme: 

.. code-block:: python

	html_theme = 'sphinx_rtd_theme'

|

-----------------

how to change the max-width of sphinx_rtd_theme
'''''''''''''''''''''''''''''''''''''''''''''''''''''

reference: 

- https://stackoverflow.com/questions/23211695/modifying-content-width-of-the-sphinx-theme-read-the-docs

- Open ``C:/Python27/Lib/site-packages/sphinx_rtd_theme/static/css/theme.css``
    
::

    .wy-nav-content {
        padding: 1.618em 3.236em;
        height: 100%;
        max-width: 800px;
        margin: auto;
    }

- remove    ``max-width: 800px;``

|

-----------------

Hide     View Page source  
'''''''''''''''''''''''''''''''''''''''''''

add the flowing python code to the conf.py file 

.. code-block:: python

    html_show_sourcelink = False

|

--------------------

Add Logo
'''''''''''''''''''''''''''''

- add the flowing python code to the conf.py file 
- copy the logo.jpg file to the html/_sources/images/logo.jpg

.. code-block:: python

    html_logo = '_sources/images/logo.jpg'
    html_theme_options = {
        'logo_only': False,
        'display_version': False,
    }


|
|
|

------------------

reStructuredText Preview Tools:
````````````````````````````````````````

|

Online Preview:

- http://socrates.io/#fjhNh8r
- http://rst.ninjs.org/#
- http://rst.aaroniles.net/

|

Visual Studio Code  plugin:

- https://marketplace.visualstudio.com/items?itemName=tht13.rst-vscode

|
|
|

-----------------


Convert markdown to reStructuredText
``````````````````````````````````````````

install Pandoc
''''''''''''''''''

    Download Page:   https://github.com/jgm/pandoc/releases/tag/2.7.1


|

-----------------


Convert .md -> .rst
''''''''''''''''''''''''''

::

    pandoc --from=markdown --to=rst --output=filename.rst filename.md


|

-----------------


Convert .rst -> .md
''''''''''''''''''''''''

::

    pandoc -f rst -t markdown -o filename.md filename.rst

|
|
|

-----------------


Install mermaid to Use (flowchart, diagram, gantchart)
```````````````````````````````````````````````````````````````
|


install mermaid
'''''''''''''''''

- https://github.com/mgaitan/sphinxcontrib-mermaid

You can install it using pip

.. code-block:: python

    pip install sphinxcontrib-mermaid


Then add sphinxcontrib.mermaid in extensions list of your projec't conf.py:

.. code-block:: python

    extensions = [
    ...,
    'sphinxcontrib.mermaid'
    ]


how to use mermaid
''''''''''''''''''''

- https://mermaidjs.github.io/


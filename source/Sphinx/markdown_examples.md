# Markdown Example


```
# Markdown Example

## H2
### H3
#### H4
##### H5
###### H6
```

## H2
### H3
#### H4
##### H5
###### H6

<br>

## Italic

```
Emphasis, aka italics, with *asterisks* or _underscores_.
```

Emphasis, aka italics, with *asterisks* or _underscores_.

<br>

## Bold 

```
Strong emphasis, aka bold, with double **asterisks** or __underscores__.
```

Strong emphasis, aka bold, with double **asterisks** or __underscores__.

<br>

## Bold with Italic

```
Combined emphasis with **asterisks and _underscores_**.
```

Combined emphasis with **asterisks and _underscores_**.


<br>

## Scratch 

```
<del>Scratch this</del>
```

<del>Scratch this</del>

<br>

## Mark

```
<mark>mark me</mark>
```

<mark>mark me</mark>


<br>

```
- fruit
  - banana
  - mango
  - pitaya
```

- fruit
  - banana
  - mango
  - pitaya


<br>

```
- `#F00`
- `#F00A`
- `#FF0000`
- `#FF0000AA`
- `RGB(0,255,0)`
- `RGB(0%,100%,0%)`
- `RGBA(0,255,0,0.3)`
- `HSL(540,70%,50%)`
- `HSLA(540,70%,50%,0.3)`
```

- `#F00`
- `#F00A`
- `#FF0000`
- `#FF0000AA`
- `RGB(0,255,0)`
- `RGB(0%,100%,0%)`
- `RGBA(0,255,0,0.3)`
- `HSL(540,70%,50%)`
- `HSLA(540,70%,50%,0.3)`

<br>

```
Inline `code` has `back-ticks around` it.
```

Inline `code` has `back-ticks around` it.



<br>

```
>>>

If you paste a message from somewhere else

that spans multiple lines,

you can quote that without having to manually prepend `>` to every line!

>>>
```

>>>

If you paste a message from somewhere else

that spans multiple lines,

you can quote that without having to manually prepend `>` to every line!

>>>


<br>

## Link

```
[markdown_Page](./markdown_test.md)
```

[markdown_Page](./markdown_test.md)


<br>


## Table

```
| header 1 | header 2 | header 3 |
| ---      |  ------  |---------:|
| cell 1   | cell 2   | cell 3   |
| cell 4 | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It will eventually wrap the text when the cell is too large for the display size. |
| cell 7   |          | cell <br> 9 |
```

| header 1 | header 2 | header 3 |
| ---      |  ------  |---------:|
| cell 1   | cell 2   | cell 3   |
| cell 4 | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It will eventually wrap the text when the cell is too large for the display size. |
| cell 7   |          | cell <br> 9 |


<br>


## source code

<br>

### Java


`````
```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```
`````


```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

<br>

### python

`````
```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```
`````

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```


<br>

### ruby

`````
```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```
`````

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

<br>

### text

`````
```
No language indicated, so no syntax highlighting.
s = "There is no highlighting for this."
But let's throw in a <b>tag</b>.
```
`````


```
No language indicated, so no syntax highlighting.
s = "There is no highlighting for this."
But let's throw in a <b>tag</b>.
```


<br>

## Task lists

```
- [X] item 1
    * [X] item A
    * [ ] item B
        more text
        + [x] item a
        + [ ] item b
        + [x] item c
    * [X] item C
- [ ] item 2
- [ ] item 3
```


- [X] item 1
    * [X] item A
    * [ ] item B
        more text
        + [x] item a
        + [ ] item b
        + [x] item c
    * [X] item C
- [ ] item 2
- [ ] item 3





<br>

## Emoji

```
Sometimes you want to :monkey: around a bit and add some :star2: to your :speech_balloon:. Well we have a gift for you:

:zap: You can use emoji anywhere GFM is supported. :v:

You can use it to point out a :bug: or warn about :speak_no_evil: patches. And if someone improves your really :snail: code, send them some :birthday:. People will :heart: you for that.

If you are new to this, don't be :fearful:. You can easily join the emoji :family:. All you need to do is to look up one of the supported codes.

Consult the [Emoji Cheat Sheet](https://www.emojicopy.com) for a list of all supported emoji codes. :thumbsup:
```

Sometimes you want to :monkey: around a bit and add some :star2: to your :speech_balloon:. Well we have a gift for you:

:zap: You can use emoji anywhere GFM is supported. :v:

You can use it to point out a :bug: or warn about :speak_no_evil: patches. And if someone improves your really :snail: code, send them some :birthday:. People will :heart: you for that.

If you are new to this, don't be :fearful:. You can easily join the emoji :family:. All you need to do is to look up one of the supported codes.

Consult the [Emoji Cheat Sheet](https://www.emojicopy.com) for a list of all supported emoji codes. :thumbsup:




<br>

## Math

```
$`a^2+b^2=c^2`$.
```

$`a^2+b^2=c^2`$.


<br>

## Image

```
![picture](../Sphinx/_sources/images/ball1.gif)
```

![picture](../Sphinx/_sources/images/ball1.gif)

<br>

```
![alt text1][logo]
[logo]: ../Sphinx/_sources/images/ball1.gif
```

![alt text1][logo]
[logo]: ../Sphinx/_sources/images/ball1.gif



<br>
<br>

## Video

### Youtube

```
[!embed](https://www.youtube.com/watch?v=YE7VzlLtp-4)
```

[!embed](https://www.youtube.com/watch?v=YE7VzlLtp-4)

<br>

### Local

```
<video controls src="../_static/videos/big_buck_bunny.mp4" width=70%></video>
```

<video controls src="../_static/videos/big_buck_bunny.mp4" width=70%></video>

<br>


## HTML

```
<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. HTML <em>tags</em> will <b>work</b>, in most cases.</dd>
</dl>
```

<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. HTML <em>tags</em> will <b>work</b>, in most cases.</dd>
</dl>


<br>
<br>

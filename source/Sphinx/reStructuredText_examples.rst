======================================
ReStructuredText Example
======================================

|
|
|


.. contents::



General formatting
--------------------

|

::

    Paragraphs that spread across
    multiple lines in the source file
    will display on one line in the
    built html file.

    The source file uses two line breaks
    to indicate a paragraph break.

Paragraphs that spread across
multiple lines in the source file
will display on one line in the
built html file.

The source file uses two line breaks
to indicate a paragraph break.

|
|
|

::

    .. note:: This is a note admonition.
        This is the second line of the first paragraph.

        - The note contains all indented body elements
        following.
        - It includes this bullet list.


.. note:: This is a note admonition.
    This is the second line of the first paragraph.

    - The note contains all indented body elements
      following.
    - It includes this bullet list.

|
|
|

::

    *italic* 
    **bold** 
    ``literal``k

- *italic* 

- **bold** 

- ``literal``

|
|
|

::

    a :emphasis:`emphasis` b :strong:`strong` c :literal:`literal`
    d :subscript:`subscript` e :superscript:`superscript` f
    :title-reference:`title-reference` g

a :emphasis:`emphasis` b :strong:`strong` c :literal:`literal`
d :subscript:`subscript` e :superscript:`superscript` f
:title-reference:`title-reference` g

|
|
|

Smart quotes, dashes, and Unicode
----------------------------------------

::

    abc--def |reg| ghi "jkl" 'mno'
    abc--def |copy| ghi "jkl" 'mno'

    .. |reg|    unicode:: U+000AE .. REGISTERED SIGN
    .. |copy|   unicode:: U+000A9 .. COPYRIGHT SIGN

abc--def |reg| ghi "jkl" 'mno'

abc--def |copy| ghi "jkl" 'mno'

.. |reg|    unicode:: U+000AE .. REGISTERED SIGN
.. |copy|   unicode:: U+000A9 .. COPYRIGHT SIGN

|
|
|


Lists and indentation
--------------------------

|

::

    * This is a bulleted list.
    * It has two items, the second
    item uses two lines. (note the indentation)

    1. This is a numbered list.
    2. It has two items too.

    #. This is a numbered list.
    #. It has two items too.

* This is a bulleted list.
* It has two items, the second
  item uses two lines. (note the indentation)

1. This is a numbered list.
2. It has two items too.

#. This is a numbered list.
#. It has two items too.

|
|
|


More complex lists and indentation
----------------------------------------

|

::

    #. Item 1.

    #. Item 2.

    More text.

    #. Part A::

            command block

        OR::

            alternate command block

    #. Part B.

    #. Item 3. This item can have a long paragraph across multiple lines. One two
    three four five six seven eight nine ten eleven twelve thirteen fourteen
    fifteen sixteen seventeen eighteen nineteen twenty.

    - Option A::

            command block

    - Option B::

            alternate command block


#. Item 1.

#. Item 2.

   More text.

   #. Part A::

          command block

      OR::

          alternate command block

   #. Part B.

#. Item 3. This item can have a long paragraph across multiple lines. One two
   three four five six seven eight nine ten eleven twelve thirteen fourteen
   fifteen sixteen seventeen eighteen nineteen twenty.

   - Option A::

         command block

   - Option B::

         alternate command block

|
|
|

Code blocks
-----------------

|

rst
`````

::

    .. code-block:: rst

        This is a paragraph split across
        two lines.

.. code-block:: rst

    This is a paragraph split across
    two lines.

python
`````````

::

    .. code-block:: python

        for i in range(10):
            print(i)

.. code-block:: python

    for i in range(10):
        print(i)


html
````````

::

    .. code-block:: html

        <html>
            <head>Hello!</head>
            <body>Hello, world!</body>
        </html>

.. code-block:: html

    <html>
        <head>Hello!</head>
        <body>Hello, world!</body>
    </html>

yaml
``````

::

    .. code-block:: yaml

        envs_dirs:
        - ~/my-envs
        - /opt/anaconda/envs

.. code-block:: yaml

    envs_dirs:
      - ~/my-envs
      - /opt/anaconda/envs

bash
```````

::

    .. code-block:: bash

        ls
        pwd
        touch a.txt

.. code-block:: bash

    ls
    pwd
    touch a.txt


none
``````````

::

.. code-block:: none

    cat program.py

    for i in range(10):
        print(i)

.. code-block:: none

    cat program.py

    for i in range(10):
        print(i)



Captions

::

    .. code-block:: python
        :caption: this.py
        :name: this-py

        print('Explicit is better than implicit.')

.. code-block:: python
    :caption: this.py
    :name: this-py

    print('Explicit is better than implicit.')


|
|
|


Tables
-----------

|

::

    .. table::
        :widths: 40 20 80
        
        +----------+-----------+-------+
        | a        | b         | c     |
        +==========+===========+=======+
        | north    | north     | north |
        | west     |           | east  |
        +----------+-----------+-------+
        | west     | center    | east  |
        +----------+-----------+-------+
        | south    | south     | south |
        |          |           |       |
        | west     |           | east  |
        +----------+-----------+-------+

.. table::
    :widths: 40 20 80

    +----------+-----------+-------+
    | a        | b         | c     |
    +==========+===========+=======+
    | north    | north     | north |
    | west     |           | east  |
    +----------+-----------+-------+
    | west     | center    | east  |
    +----------+-----------+-------+
    | south    | south     | south |
    |          |           |       |
    | west     |           | east  |
    +----------+-----------+-------+


|
|
|


::

    +----------+-----------+-------+
    | north    | north     | north |
    | west     |           | east  |
    +----------+-----------+-------+
    | west     | center    | east  |
    +----------+-----------+-------+
    | south    | south     | south |
    | west     |           | east  |
    +----------+-----------+-------+

+----------+-----------+-------+
| north    | north     | north |
| west     |           | east  |
+----------+-----------+-------+
| west     | center    | east  |
+----------+-----------+-------+
| south    | south     | south |
| west     |           | east  |
+----------+-----------+-------+


|
|
|




::

    =====  =====  =======
    A      B      A and B
    =====  =====  =======
    False  False  False
    True   False  False
    False  True   False
    True   True   True
    =====  =====  =======

=====  =====  =======
A      B      A and B
=====  =====  =======
False  False  False
True   False  False
False  True   False
True   True   True
=====  =====  =======


|
|
|

::

    .. list-table:: Title
    :widths: 25 25 50
    :header-rows: 1

    * - Heading row 1, column 1
        - Heading row 1, column 2
        - Heading row 1, column 3
    * - Row 1, column 1
        -
        - Row 1, column 3
    * - Row 2, column 1
        - Row 2, column 2
        - Row 2, column 3


.. list-table:: Title
   :widths: 25 25 50
   :header-rows: 1

   * - Heading row 1, column 1
     - Heading row 1, column 2
     - Heading row 1, column 3
   * - Row 1, column 1
     -
     - Row 1, column 3
   * - Row 2, column 1
     - Row 2, column 2
     - Row 2, column 3



|
|
|


contents
-----------

::

    .. contents::

.. contents::


Adding the ``:local:`` option removes the page title “Detailed reStructuredText and Sphinx example file” and the table of contents title “Contents”. You can see how it displays at the top of this file.

::

    .. contents::
        :local:

Another file might use this table of contents:

::

    .. contents:: Table of Contents
        :depth: 2


|
|
|


Index files
--------------

|

Instead of using the ``contents`` directive to show a table of its own contents, the index file uses the ``toctree`` directive to show a table of other files. All files in the archive should be reachable from the toctrees in the index. Files can also contain toctrees of their own, which can lead to other files not referenced directly by the index. Toctrees may be hidden, in which case they will be used to build the left navigation column but not appear in the main page text.

::

    .. toctree::
        :maxdepth: 2

        file-one
        file-two
        file-three



|
|
|

Notice
------------

|

::

    .. seealso:: This is a simple **seealso** note.

.. seealso:: This is a simple **seealso** note.


|
|
|


::

    .. note::  This is a **note** box.

.. note::  This is a **note** box.


|
|
|


::

    .. warning:: note the space between the directive and the text

.. warning:: note the space between the directive and the text


|
|
|


::

    .. topic:: Your Topic Title

    Subsequent indented lines comprise
    the body of the topic, and are
    interpreted as body elements.


.. topic:: Your Topic Title

    Subsequent indented lines comprise
    the body of the topic, and are
    interpreted as body elements.


|
|
|

sidebar
-------------

::

    .. sidebar:: Sidebar Title
    :subtitle: Optional Sidebar Subtitle

    Subsequent indented lines comprise
    the body of the sidebar, and are
    interpreted as body elements.

.. sidebar:: Sidebar Title
    :subtitle: Optional Sidebar Subtitle

    Subsequent indented lines comprise
    the body of the sidebar, and are
    interpreted as body elements.


|
|
|


math
----------

::

    :math:`\alpha > \beta`

:math:`\alpha > \beta`

|
|
|

::

    .. math::

        n_{\mathrm{offset}} = \sum_{k=0}^{N-1} s_k n_k


.. math::

    n_{\mathrm{offset}} = \sum_{k=0}^{N-1} s_k n_k


|
|
|


Flowchart
--------------

|

::

    .. mermaid::

        graph TD;
            A-->B;
            A-->C;
            B-->D;
            C-->D;

.. mermaid::

    graph TD;
        A-->B;
        A-->C;
        B-->D;
        C-->D;

|
|
|

Diagram 
-----------

|

::

    .. mermaid::

        sequenceDiagram
            participant Alice
            participant Bob
            Alice->John: Hello John, how are you?
            loop Healthcheck
                John->John: Fight against hypochondria
            end
            Note right of John: Rational thoughts <br/>prevail...
            John-->Alice: Great!
            John->Bob: How about you?
            Bob-->John: Jolly good!
            
.. mermaid::

    sequenceDiagram
        participant Alice
        participant Bob
        Alice->John: Hello John, how are you?
        loop Healthcheck
            John->John: Fight against hypochondria
        end
        Note right of John: Rational thoughts <br/>prevail...
        John-->Alice: Great!
        John->Bob: How about you?
        Bob-->John: Jolly good!

|
|
|


Gantt Chart
-----------

|

::

    .. mermaid::

        gantt
            dateFormat  YYYY-MM-DD
            title Adding GANTT diagram functionality to mermaid
            section A section
            Completed task            :done,    des1, 2014-01-06,2014-01-08
            Active task               :active,  des2, 2014-01-09, 3d
            Future task               :         des3, after des2, 5d
            Future task2               :         des4, after des3, 5d
            section Critical tasks
            Completed task in the critical line :crit, done, 2014-01-06,24h
            Implement parser and jison          :crit, done, after des1, 2d
            Create tests for parser             :crit, active, 3d
            Future task in critical line        :crit, 5d
            Create tests for renderer           :2d
            Add to mermaid                      :1d

.. mermaid::

    gantt
        dateFormat  YYYY-MM-DD
        title Adding GANTT diagram functionality to mermaid
        section A section
        Completed task            :done,    des1, 2014-01-06,2014-01-08
        Active task               :active,  des2, 2014-01-09, 3d
        Future task               :         des3, after des2, 5d
        Future task2               :         des4, after des3, 5d
        section Critical tasks
        Completed task in the critical line :crit, done, 2014-01-06,24h
        Implement parser and jison          :crit, done, after des1, 2d
        Create tests for parser             :crit, active, 3d
        Future task in critical line        :crit, 5d
        Create tests for renderer           :2d
        Add to mermaid                      :1d


|
|
|


Images
-----------

|


::

    |ball|

    ..  |ball| image:: _sources/images/ball1.gif
               :width: 10 px

|ball|

..  |ball| image:: _sources/images/ball1.gif
            :width: 10 px

|
|
|

::

    .. figure::  _sources/images/ball1.gif
        :align:   center

        bla bla.



.. figure::  _sources/images/ball1.gif
    :align:   center

    bla bla.





|
|
|

::

    |ball2|

    ..  |ball2| image:: _sources/images/ball1.gif
                :width: 20%


|ball2|

..  |ball2| image:: _sources/images/ball1.gif
            :width: 20%


|
|
|


Links
-----------

|

::

    http://microsoft.com

    `Google <http://google.com>`_

    This paragraph links to `the yahoo site`_.

    .. _the yahoo site: http://yahoo.com/

    index_

    .. _index: index.html

http://microsoft.com

`Google <http://google.com>`_

This paragraph links to `the yahoo site`_

.. _the yahoo site: http://yahoo.com/

index_

.. _index: index.html



|
|
|


Videos - Web
----------------

|

::

    .. raw:: html

        <div style="text-align: left; margin-bottom: 2em;">
        <iframe width="100%" height="350" src="https://www.youtube.com/embed/oJsUvBQyHBs?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>

.. raw:: html

    <div style="text-align: left; margin-bottom: 2em;">
    <iframe width="600" height="350" src="https://www.youtube.com/embed/oJsUvBQyHBs?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>

|
|
|

Videos - Local
----------------------

|

::

    .. raw:: html

        <video controls src="_videos/big_buck_bunny.mp4" width=80%></video>   



.. raw:: html

    <video controls src="_videos/big_buck_bunny.mp4" width=80%></video>  


|
|
|



Download file
----------------------

::

    :download:`Download monokai theme <_static/jupyter_theme/monokai/custom.css>`


    Structure:

        source/Jupyter/_static/jupyter_theme/monokai/custom.css

        source/_static/

        build/html/_downloads/31cf58193621bbdd1bf96efbcf445d81/custom.css





|
|
|

::

    ===============
    Section Title
    ===============

    ---------------
    Section Title
    ---------------

    Section Title
    =============

    Section Title
    -------------

    Section Title
    `````````````

    Section Title
    '''''''''''''

    Section Title
    .............

    Section Title
    ~~~~~~~~~~~~~

    Section Title
    *************

    Section Title
    +++++++++++++

    Section Title
    ^^^^^^^^^^^^^


|
|
|



reference

    https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst
    https://docs.anaconda.com/restructuredtext/detailed/
    https://pythonhosted.org/an_example_pypi_project/sphinx.html
    https://sublime-and-sphinx-guide.readthedocs.io/en/latest/tables.html
    https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html#colored-boxes-note-seealso-todo-and-warnings
    http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html

    https://mermaidjs.github.io/
    
